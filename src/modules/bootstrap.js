import $ from 'jquery'
import loginCheck from './loginCheck'
import ErrorSnack from './html/ErrorSnack'
import UploadCard from './html/UploadCard'

const setInitialListeners = () => {
  $('#upload-btn').on('click', () => {
    const auth2 = window.gapi.auth2.getAuthInstance()

    if (auth2.isSignedIn.get()) {
      const dialog = $('#dialog')
      const card = new UploadCard()
      dialog.append(card.el)
      dialog.removeClass('display-none')
    } else {
      const snack = new ErrorSnack('You must be signed in to upload.')
      snack.render()
    }
  })
}

export default () => {
  loginCheck()
  setInitialListeners()
}
