export const consoleWarn = (text, prefix) => {
  if (typeof text === 'string')
    text = '[' + prefix + '] ' + text

  console.warn(text)
}

export const standardAlert = (message) => {
  alert(message)
}
