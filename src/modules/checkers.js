export const isString = str => typeof str === 'string'

export const isNonEmptyString = str => isString(str) && str !== ''
