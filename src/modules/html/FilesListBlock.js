import $ from 'jquery'
import Title from './Title'

export default class FilesListBlock {
  constructor () {
    this.el = $('<div class="files-list-block bg-dark-grey pa-lg">' + Title('Current files', true) + '</div>')

    // Preparing the table to be populated later
    this.setTable()

    // Preparing to store the uploaded files locally
    this.files = []
  }

  setTable () {
    this.table = $(`<table class="full-width"></table>`)
    this.table.append(`<thead class="color-white">
      <tr>
        <th class="text-left">
          <small>Date</small>
        </th>
        <th class="text-left">
          <small>Name</small>
        </th>
        <th class="text-left">
          <small>Progress</small>
        </th>
      </tr>
    </thead>`)
    this.tbody = $(`<tbody></tbody>`)
    this.table.append(this.tbody)

    this.el.append(this.table)
  }

  addFile (file) {
    const today = new Date()

    const obj = {
      el: $(`<tr class="color-white">
        <td class="text-left">
          <small>${today.toLocaleDateString()}</small>
        </td>
        <td class="text-left">
          <small>${file.data.name}</small>
        </td>
        <td class="text-left">
          <small class="progress-value">${file.uploadProgress}%</small>
        </td>
      </tr>`),
      data: file
    }

    file.onProgressChange = (value) => {
      obj.el.find('.progress-value').text(value + '%')
    }

    this.tbody.append(obj.el)
    this.files.push(obj)
  }
}
