export default (text, isDark) => '<h5 class="title pa-md' + (isDark ? ' color-white' : '') + '">' + text + '</h5>'
