import Snack from './Snack'

export default class SuccessSnack extends Snack {
  constructor(message) {
    super(message)
    this.el.addClass('bg-green')
  }
}
