import $ from 'jquery'

export default class Input {
  constructor (title) {
    this.el = $('<div><p class="mb-xs"><small><em>' + (title ?? '') + '</em></small></p></div>')
  }
}
