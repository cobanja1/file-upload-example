import $ from 'jquery'
import Input from './Input'

export default class PwdInput extends Input {
  constructor () {
    super('Password to encrypt files')

    this.setInput()
  }

  setInput () {
    this.input = $('<input id="pwd-input" type="password" />')
    this.el.append(this.input)
  }

  getValue() {
    return this.input.val()
  }
}
