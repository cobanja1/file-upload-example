import $ from 'jquery'

export default class Snack {
  constructor (message) {
    this.el = $('<div class="snack pa-md">' + message + '</div>')
    this.el.addClass('color-white')
  }

  render () {
    $('body').append(this.el)

    setTimeout(() => {
      this.el.remove()
    }, 3000)
  }
}
