import $ from 'jquery'
import PwdInput from './PwdInput'
import FileInput from './FileInput'
import Title from './Title'

export default class InputsBlock {
  constructor () {
    this.el = $('<div class="card-inputs-block pa-lg">' + Title('File upload') + '</div>')

    this.setPwdInput()
    this.setFileInput()

    // Preparing the function to be called when the files list is updated
    this.update = null
  }

  setPwdInput () {
    this.pwdInput = new PwdInput()
    this.el.append(this.pwdInput.el)
  }

  setFileInput () {
    this.fileInput = new FileInput()
    this.el.append(this.fileInput.el)

    this.fileInput.onNewFile = (file) => {
      // Keeping the file decrypted for local use
      this.addNewFile(file)
    }
  }

  addNewFile (file) {
    this.triggerAdd(file)
  }

  // This one should be called every time the files list is changed
  triggerAdd (file) {
    if (typeof this.onAdd === 'function')
      this.onAdd(file)
  }

  getPassword () {
    return this.pwdInput.getValue()
  }
}
