import $ from 'jquery'
import InputsBlock from './InputsBlock'
import FilesListBlock from './FilesListBlock'
import { uploadFile } from '../../apis/server'

function closeDialog () {
  const dialog = $('#dialog')

  dialog.empty()
  dialog.addClass('display-none')
  dialog.off('click')
}

export default class UploadCard {
  constructor () {
    this.el = $(`<div class="card flex relative-position"></div>`)

    this.appendCardDismiss()

    // Settings inputs and list block. Order matters here, as we connect them to each other
    this.appendInputsBlock()
    this.appendFilesListBlock()

    this.activateBackdrop()

    this.filesList = []
  }

  appendCardDismiss () {
    this.cardDismiss = $('<div class="card-dismiss"><img src="/assets/img/cancel.png" /></div>')
    this.el.append(this.cardDismiss)

    this.cardDismiss.on('click', closeDialog)
  }

  appendInputsBlock () {
    this.inputsBlock = new InputsBlock()
    this.el.append(this.inputsBlock.el)
  }

  appendFilesListBlock () {
    this.filesListBlock = new FilesListBlock()
    this.el.append(this.filesListBlock.el)

    this.inputsBlock.onAdd = (file) => {
      this.filesListBlock.addFile(file)
      uploadFile(file, this.inputsBlock.getPassword())
    }
  }

  activateBackdrop () {
    $('#dialog').on('click', function ({ target }) {
      if (target !== this)
        return

      closeDialog()
    })
  }
}
