import Snack from './Snack'

export default class ErrorSnack extends Snack {
  constructor(message) {
    super(message)
    this.el.addClass('bg-red')
  }
}
