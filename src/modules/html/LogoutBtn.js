import $ from 'jquery'
import { renderLoginButton } from '../../apis/google-auth'
import SuccessSnack from './SuccessSnack'

export default class LogoutBtn {
  constructor () {
    this.el = $('<button class="button full-width color-grey">Log out</button>')
    this.el.on('click', () => {
      window.gapi.auth2.getAuthInstance().signOut().then(() => {
        const snack = new SuccessSnack('Signed out.')
        snack.render()

        $('#login').empty()
        renderLoginButton()
      })
    })
  }
}
