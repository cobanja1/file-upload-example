import $ from 'jquery'
import UploadFile from '../../models/UploadFile'
import Input from './Input'
import { isNonEmptyString } from '../checkers'
import ErrorSnack from '../html/ErrorSnack'

export default class FileInput extends Input {
  constructor () {
    super()
    this.setInput()

    // Preparing the function to be called when the file is selected
    this.onNewFile = null
  }

  setWrapper () {
    this.wrapper = $(`<div class="mt-lg"></div>`)
    this.wrapperLabel = $('<label for="file-input" class="button non-selectable">Upload File</label>')

    this.wrapper.append(this.wrapperLabel)
    this.el.append(this.wrapper)

    const self = this

    this.wrapperLabel.on('click', function (e) {
      const password = self.el.parent().find('#pwd-input').val()

      if (this !== e.target) {
        e.preventDefault()
        return
      }

      if (!isNonEmptyString(password)) {
        const snack = new ErrorSnack('Please provide the password first...')
        snack.render()

        e.preventDefault()
        return
      }
    })
  }

  setInput () {
    const self = this
    this.setWrapper()

    this.input = $('<input id="file-input" type="file" class="display-none" disable="disable" />')
    this.wrapper.append(this.input)

    this.input.on('change', function ({ target }) {
      if (!target.files.length > 0)
        return

      const file = new UploadFile(target.files[0])

      if (file.isValid() && typeof self.onNewFile === 'function')
        self.onNewFile(file)
    })
  }
}
