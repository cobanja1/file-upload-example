import { lib, PBKDF2, AES } from 'crypto-js'

// Attaching the salt to the client's current tab for the sake of the example scope
const salt = lib.WordArray.random(128 / 8)

const getKey = (password) => {
  return PBKDF2(password, salt, {
    keySize: 512 / 32,
    iterations: 1000
  })
}

export const encrypt = (text, password) => {
  const key = getKey(password)
  return AES.encrypt(text, key.toString())
}

export const decrypt = (encr, password) => {
  const key = getKey(password)
  return AES.decrypt(encr, key.toString())
}
