import $ from 'jquery'
import { loadGoogle } from './api-loader'
import { consoleWarn, standardAlert } from '../modules/console'
import SuccessSnack from '../modules/html/SuccessSnack'
import LogoutBtn from '../modules/html/LogoutBtn'

const onLoginSuccess = () => {
  const snack = new SuccessSnack('Signed in with Google')
  snack.render()

  $('#login').empty()
  renderLogoutButton()
}

const onLoginFailure = () => {
  standardAlert('Cannot sign in with Google now...')
}

const renderLogoutButton = () => {
  const btn = new LogoutBtn()
  $('#login').append(btn.el)
}

export const renderLoginButton = () => {
  window.gapi.signin2.render(
    'login',
    {
      'scope': 'profile',
      'width': 240,
      'height': 40,
      'longtitle': true,
      'onsuccess': onLoginSuccess,
      'onfailure': onLoginFailure
    }
  )
}

const renderLogin = () => {
  const auth2 = window.gapi.auth2.getAuthInstance()

  if (auth2.isSignedIn.get()) {
    const currentUser = auth2.currentUser.get()
    const profile = currentUser.getBasicProfile()

    $('#login').empty()
    renderLogoutButton()
  } else {
    renderLoginButton()
  }
}

const authInit = () => {
  const client_id = import.meta.env.VITE_GOOGLE_CLID

  if (!client_id) {
    consoleWarn('Google API key is not set', 'Auth')
    return
  }

  window.gapi.load('auth2', () => {
    window.gapi.auth2
      .init({ client_id })
      .then(renderLogin)
  })
}

export default () => {
  loadGoogle().then(authInit)
}
