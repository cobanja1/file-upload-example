const loadScript = (path, resolve) => {
  const scr = document.createElement('script')
  scr.src = path
  scr.async = true

  scr.onload = () => {
    resolve()
  }

  document.head.append(scr)
}

export const loadGoogle = () => new Promise((resolve) => {
  loadScript('https://apis.google.com/js/platform.js', resolve)
})

