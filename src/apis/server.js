import { encrypt, decrypt } from './Encryption'
import SuccessSnack from '../modules/html/SuccessSnack'
import { enc } from 'crypto-js'

const getDataUri = (file) => new Promise((resolve, reject) => {
  const reader = new FileReader()
  reader.onload = function () {
    resolve(reader.result)
  }
  reader.onerror = reject

  reader.readAsDataURL(file)
})

// Making sure this function is not blocking the thread
export const uploadFile = async (file, password) => {
  // Here, we are mocking the backend request/response
  // The file is supposed to be encrypted before the upload
  // The JWT token is supposed to be passed in Authorization header and it is supposed to be verified on the server side before sending the success response
  // The file size is supposed to be checked against size limits specified in .env file

  file.uploadProgress = 5

  getDataUri(file.data).then((dataUri) => {
    console.log("-\n-\nEncrypting file. Source: ")
    console.log(dataUri)

    const [mimeType, data] = dataUri.split(',')
    const encrypted = encrypt(enc.Base64.parse(data), password)
    const auth2 = window.gapi.auth2.getAuthInstance()

    console.log("-\n-\nSending AES encrypted: ")
    console.log(encrypted.toString())

    console.log("-\n-\nJWT to provide and verify on the server is: ")
    console.log(auth2.currentUser.get().getAuthResponse().id_token)

    file.uploadProgress = 10
    let uploadedSize = 0 // Mocking xhr.onprogress event with this value
    const timeChunk = 300
    const sizeChunk = timeChunk * 1000

    const interval = setInterval(() => {  
      uploadedSize += sizeChunk
      file.uploadProgress = parseInt(10 + (uploadedSize / file.data.size) * 90)

      if (uploadedSize >= file.data.size) {
        file.uploadProgress = 100
        clearInterval(interval)

        const snack = new SuccessSnack('File is uploaded!')
        snack.render()
    
        const decrypted = decrypt(encrypted.toString(), password)

        console.log("-\n-\nDecoded DataUri:")
        console.log(mimeType + ',' + enc.Base64.stringify(decrypted))
      }
    }, timeChunk)
  })
}
