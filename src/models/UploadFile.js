const minSize = parseInt(import.meta.env.VITE_FILE_MIN_SIZE ?? 0)
const maxSize = parseInt(import.meta.env.VITE_FILE_MAX_SIZE ?? 0)

export default class UploadFile {
  _uploadProgress = 0
  onProgressChange = null

  constructor (file) {
    this.data = file
  }

  get uploadProgress () { return this._uploadProgress }
  set uploadProgress (value) {
    if (value < 0)
      value = 0
    else if (value > 100)
      value = 100

    this._uploadProgress = value

    if (typeof this.onProgressChange === 'function')
      this.onProgressChange(value)
  }

  isValidSize () {
    return this.data.size >= minSize
      && this.data.size <= maxSize
  }

  isValid () {
    return this.isValidSize()
  }
}
