# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This is an example of the application for uploading the files to the backend. Please note that the files are not really stored on the backend - it is just a mocking of the server-side behaviour.
* Version 0.0.1

### Tech stack used
- Vanilla javascript + jQuery (HTML and CSS orchestration);
- HTML5 FileReader API;
- Vite (hot module reload);
- Google API (user authentication);
- CryptoJS for encrypting/decrypting files data;

### How do I set this example up? ###

* Clone the repository to the folder of your choice
* set .env config file (see below)
* `$ cd your-folder`
* `$ npm install`
* `$ npm run dev`

### .env variables
``` .env
VITE_GOOGLE_CLID="XXXXXXX-XXXXXXXX" # Google Client ID
VITE_FILE_MIN_SIZE=XXXXX # Min. upload size in bytes
VITE_FILE_MAX_SIZE=XXXXXXX # Max. upload size in bytes
```

### Building the project
`$ npm run build`
